class JobsController < ApplicationController
  
  add_breadcrumb "Вакансии", :jobs_path

  def show
    @job = Job.find params[:id]
    add_breadcrumb @job.title if @job

  end
end
