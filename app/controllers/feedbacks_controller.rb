class FeedbacksController < ApplicationController
  def new
  end
  def create
      respond_to do |format|
        feedback = Feedback.new feedback_params
        if feedback.save!
          format.js {
            @message = "Заявка отправлена"
            #UserMailer.call_to_order(feedback).deliver
          }
        else
          format.js { render json: current_user.profile.feedbacks.errors, status: :unprocessable_entity }
        end
    end
  end
  
  def feedback_params
    params.require(:feedback).permit(:title, :from, :phone, :email, :text)
  end
end
