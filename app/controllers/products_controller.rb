class ProductsController < ApplicationController
  
  add_breadcrumb "Продукция" , :products_path

  def index
    @products = Product.all
  end
end
