class WelcomeController < ApplicationController
  def index
    @factories = FactoryOil.all.active.on_main
    @news = News.all.active.order('id desc').limit(3)
    
    @from_city = Delivery.all.group(:from)
    @to_city = Delivery.all.group(:to)
    @trucks = Delivery.all.group(:truck)
    
    first_order_summ = Delivery.where("`to` = ? AND `from`= ? AND `truck` = ?",
    @to_city.first.to , @from_city.first.from, @trucks.first.truck).first
    
    @first_order_summ= first_order_summ ? first_order_summ.price_ton.to_i * first_order_summ.truck.to_i : 0
  end
end
