class NewsController < ApplicationController
  
  add_breadcrumb "Новости компании", :news_index_path
  def index
    @news = News.all.active.order('created_at desc')
  end
  def show
    @news = News.find_by_id! params[:id]
    add_breadcrumb @news.title if @news
  end 
  private
   def news_params
      params.require(:news).permit(:created_at)
    end
end
