// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui.js
//= require plugins
//= require main
//= require jquery.ui.touch-punch.min.js
//= require cusel-min-2.5.js
//= require modernizr-2.8.3-respond-1.4.2.min
//= require bootstrap
//= require jcarousel.responsive
//= require jquery.jcarousel.min
//= require jquery.mousewheel.js
//= require jquery.mCustomScrollbar.concat.min.js
//= require jquery.ui.scrollbar.min.js

$(document).ready(function() {

    var params = {
        changedEl: "select"
    }
    cuSel(params);
	
    $( ".checkbox-ui" ).button();

	$('#new_delivery .custom-select').on('change', function() {
 		$('#new_delivery').submit();
	});
	
    $(window).load(function(){
        $('.cusel-scroll-wrap, .scroll-pane').mCustomScrollbar({
            axis:"y"
        });
    });
});

jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();