class FactoryOil < ActiveRecord::Base
  has_many :product_relations
  has_many :products, :through => :product_relations
  validates :name, :fullname, :about, :address, :image, presence: true
  scope :active, -> { where is_active: true }
  scope :on_main, -> { where is_main: true }

  DEFAULT_AVATAR = 'demo/_blank.png'
  has_attached_file :image,
                    styles: { small: '250x150>', medium: '220x280#', thumb: '100x125#' },
                    default_url: DEFAULT_AVATAR,
                    url: '/system/:class/:id/:style/:basename.:extension',
                    path: ':rails_root/public/system/:class/:id/:style/:basename.:extension'

  validates_attachment_content_type :image, content_type: /^image\/(jpg|jpeg|pjpeg|png|x-png|gif)$/
end
