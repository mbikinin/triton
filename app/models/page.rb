class Page < ActiveRecord::Base
  validates :name, :uri, :text, presence: true
end
