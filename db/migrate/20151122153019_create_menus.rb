class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.string :name
      t.string :uri
      t.boolean :is_active, default: true
      t.integer :type_of, default: 0
      t.integer :sort, default: 1
      t.timestamps null: false
    end
  end
end
