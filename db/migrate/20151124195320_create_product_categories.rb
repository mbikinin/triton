class CreateProductCategories < ActiveRecord::Migration
  def change
    create_table :product_categories do |t|
      t.string :name
      t.boolean :is_active
      t.belongs_to :factory_oil, index: true
      t.timestamps null: false
    end
  end
end
