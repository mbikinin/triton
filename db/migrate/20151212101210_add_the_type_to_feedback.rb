class AddTheTypeToFeedback < ActiveRecord::Migration
  def change
    add_column :feedbacks, :the_type, :string
    add_column :feedbacks, :name, :string
  end
end
