class CreateProductRelations < ActiveRecord::Migration
  def change
    create_table :product_relations do |t|
      t.belongs_to :product, index: true
      t.belongs_to :factory_oil, index: true
      t.timestamps null: false
    end
  end
end
