class CreateFactoryOils < ActiveRecord::Migration
  def change
    create_table :factory_oils do |t|
      t.string :name
      t.string :fullname
      t.string :address
      t.string :phone
      t.text :about
      t.boolean :is_active
      t.boolean :is_main
      t.timestamps null: false
    end
    add_attachment :factory_oils, :image
  end
end
