class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :title
      t.text :announce
      t.text :text
      t.boolean :is_active
      t.timestamps null: false
    end
  end
end
