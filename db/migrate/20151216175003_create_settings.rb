class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string :site_name
      t.string :full_name
      t.text :address
      t.text :full_address
      t.string :header_phone
      t.string :footer_phone
      t.string :order_phone
      t.text :manager_phones      
      t.string :keywords
      t.string :description
      t.string :work_time
      
      t.timestamps null: false
    end
  end
end
