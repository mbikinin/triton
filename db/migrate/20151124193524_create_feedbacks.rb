class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.string :title
      t.string :from
      t.string :to
      t.text :announce
      t.text :text
      t.string :phone
      t.string :email

      t.timestamps null: false
    end
  end
end
