class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :name
      t.string :uri
      t.text :announce
      t.text :text
      t.boolean :is_active
      t.timestamps null: false
    end
  end
end
