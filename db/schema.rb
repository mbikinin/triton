# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151217192236) do

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",    limit: 255, null: false
    t.string   "data_content_type", limit: 255
    t.integer  "data_file_size",    limit: 4
    t.integer  "assetable_id",      limit: 4
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width",             limit: 4
    t.integer  "height",            limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "deliveries", force: :cascade do |t|
    t.string   "article",     limit: 255
    t.string   "from",        limit: 255
    t.string   "to",          limit: 255
    t.string   "truck",       limit: 255
    t.float    "price_liter", limit: 24
    t.float    "price_ton",   limit: 24
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "factory_oils", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.string   "fullname",           limit: 255
    t.string   "address",            limit: 255
    t.string   "phone",              limit: 255
    t.text     "about",              limit: 65535
    t.boolean  "is_active"
    t.boolean  "is_main"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
  end

  create_table "feedbacks", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.string   "from",       limit: 255
    t.string   "to",         limit: 255
    t.text     "announce",   limit: 65535
    t.text     "text",       limit: 65535
    t.string   "phone",      limit: 255
    t.string   "email",      limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "the_type",   limit: 255
    t.string   "name",       limit: 255
  end

  create_table "jobs", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.text     "text",       limit: 65535
    t.boolean  "is_active"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "menus", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "uri",        limit: 255
    t.boolean  "is_active",              default: true
    t.integer  "type_of",    limit: 4,   default: 0
    t.integer  "sort",       limit: 4,   default: 1
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "news", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.text     "announce",   limit: 65535
    t.text     "text",       limit: 65535
    t.boolean  "is_active"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "pages", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "uri",        limit: 255
    t.text     "announce",   limit: 65535
    t.text     "text",       limit: 65535
    t.boolean  "is_active"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "product_categories", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.boolean  "is_active"
    t.integer  "factory_oil_id", limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "product_categories", ["factory_oil_id"], name: "index_product_categories_on_factory_oil_id", using: :btree

  create_table "product_relations", force: :cascade do |t|
    t.integer  "product_id",     limit: 4
    t.integer  "factory_oil_id", limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "product_relations", ["factory_oil_id"], name: "index_product_relations_on_factory_oil_id", using: :btree
  add_index "product_relations", ["product_id"], name: "index_product_relations_on_product_id", using: :btree

  create_table "products", force: :cascade do |t|
    t.string   "name",                limit: 255
    t.string   "article",             limit: 255
    t.decimal  "price_liter",                       precision: 10
    t.decimal  "price_ton",                         precision: 10
    t.text     "text",                limit: 65535
    t.datetime "created_at",                                                     null: false
    t.datetime "updated_at",                                                     null: false
    t.integer  "product_category_id", limit: 4
    t.string   "image_file_name",     limit: 255
    t.string   "image_content_type",  limit: 255
    t.integer  "image_file_size",     limit: 4
    t.datetime "image_updated_at"
    t.string   "density",             limit: 255,                  default: "1"
  end

  add_index "products", ["product_category_id"], name: "index_products_on_product_category_id", using: :btree

  create_table "rich_rich_files", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "rich_file_file_name",    limit: 255
    t.string   "rich_file_content_type", limit: 255
    t.integer  "rich_file_file_size",    limit: 4
    t.datetime "rich_file_updated_at"
    t.string   "owner_type",             limit: 255
    t.integer  "owner_id",               limit: 4
    t.text     "uri_cache",              limit: 65535
    t.string   "simplified_type",        limit: 255,   default: "file"
  end

  create_table "settings", force: :cascade do |t|
    t.string   "site_name",      limit: 255
    t.string   "full_name",      limit: 255
    t.text     "address",        limit: 65535
    t.text     "full_address",   limit: 65535
    t.string   "header_phone",   limit: 255
    t.string   "footer_phone",   limit: 255
    t.string   "order_phone",    limit: 255
    t.text     "manager_phones", limit: 65535
    t.string   "keywords",       limit: 255
    t.string   "description",    limit: 255
    t.string   "work_time",      limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

end
