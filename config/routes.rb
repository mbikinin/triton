Rails.application.routes.draw do
  get 'news/index'

  mount Ckeditor::Engine => '/ckeditor'
  mount Rich::Engine => '/rich', :as => 'rich'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'
  
  get 'jobs' => 'pages#jobs'
  resources :jobs, only: [:show]
  resources :feedbacks
  resources :pages, only: [:index, :show]
  resources :products, only: [:index, :show]
  resources :deliveries
  post '/delivery/get_price' => 'deliveries#get_price', as: :get_price
  resources :news, only: [:index, :show]
  get 'contacts' => 'pages#contacts'
  get 'delivery' => 'pages#delivery'
  
  get 'products' => 'pages#products'
  get 'page/:id' => 'pages#show'
  
  get 'factories' => 'factories#index', as: :factories
  get 'factory/:id' => 'factories#show', as: :factory
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
